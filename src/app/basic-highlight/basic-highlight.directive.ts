import { Directive, ElementRef, OnInit } from "@angular/core";

@Directive({
  selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit {

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    // DON'T: not a good practice to access elements directly like this
    this.elementRef.nativeElement.style.backgroundColor = 'green';
  }
}
